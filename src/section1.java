import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class section1 {
	
		WebDriver dr;
		
		 @Before
		    public void startApp()
		{
			 System.setProperty("webdriver.chrome.driver", "D:\\ALM image\\Sel\\chromedriver.exe");
		     dr=new ChromeDriver();
			 dr.get("https://shop.demoqa.com/");
		}
		 
		 @After
		 public void CloseApp()
		 {
			 dr.close();
		 }
		
		 @Test
		 public void screenTitle()
		 {
			 List<WebElement> Links= dr.findElements(By.tagName("a"));
			 boolean b1=false;
			 for(WebElement a:Links)
			 {
				 if(a.getAttribute("href").equals("https://shop.demoqa.com/"))
				 {
					 b1=true;
					 break;
				 }
			 }
			
			 Assert.assertTrue(b1);
		 }
		 
		 @Test
		 public void link()
		 {
			 boolean b2=dr.getCurrentUrl().contains("https://shop.demoqa.com/");
		Assert.assertTrue(b2);
		 }
}
			 